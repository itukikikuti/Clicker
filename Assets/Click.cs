﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ItsGameAssets;

public class Click : MonoBehaviour
{
    [SerializeField] private Button button;
    [SerializeField] private Text priceText;
    [SerializeField] private Text levelText;
    [SerializeField] private Clicker clicker;
    [SerializeField] private AudioClip paySound;
    [SerializeField] private int basicPrice;
    private int level = 0;

    private void Start()
    {
        button.onClick.AddListener(OnClick);
        level = int.Parse(PlayerPrefs.GetString(gameObject.name, "0"));
        clicker.clickIncome = level + 1;

        priceText.text = string.Format("￥{0:#,0}", basicPrice * (level + 1));
        levelText.text = string.Format("lv {0}", level);
    }

    private void OnClick()
    {
        if (clicker.SubYen(basicPrice * (level + 1)))
        {
            level++;
            clicker.clickIncome = level + 1;
            PlayerPrefs.SetString(gameObject.name, level.ToString());

            priceText.text = string.Format("￥{0:#,0}", basicPrice * (level + 1));
            levelText.text = string.Format("lv{0}", level);

            Audio.Play(paySound, 1f, 1f);
        }
    }
}
