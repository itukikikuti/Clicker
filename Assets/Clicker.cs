﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ItsGameAssets;

public class Clicker : MonoBehaviour
{
    [SerializeField] private ParticleSystem coin;
    [SerializeField] private AudioClip clickSound;
    [SerializeField] private Text yenText;
    public event System.Action updateForSeconds = () => { };
    private ulong yen = 0;
    public int clickIncome = 1;

    private void Start()
    {
        yen = ulong.Parse(PlayerPrefs.GetString("Yen", "0"));
        updateForSeconds += Save;
        UpdateForSeconds();
    }

    private async void UpdateForSeconds()
    {
        while (true)
        {
            updateForSeconds();
            await new WaitForSeconds(1f);
        }
    }

    private void Save()
    {
        PlayerPrefs.SetString("Yen", yen.ToString());
    }

    public void AddYen(int yen)
    {
        if (yen <= 0) return;

        coin.Emit(yen);
        Audio.Play(clickSound, 1f, Random.Range(1f, 1.1f));

        transform.localScale = new Vector3(9f, 9f, 9f);
        transform.rotation = Quaternion.Euler(Random.Range(10f, -10f), 180f + Random.Range(10f, -10f), 0f);

        if (ulong.MaxValue - (ulong)yen < this.yen)
        {
            this.yen = ulong.MaxValue;
        }
        else
        {
            this.yen += (ulong)yen;
        }
    }

    public bool SubYen(int yen)
    {
        if (yen <= 0) return false;

        if (ulong.MinValue + (ulong)yen > this.yen)
        {
            return false;
        }
        else
        {
            this.yen -= (ulong)yen;
        }

        return true;
    }

    private void Update()
    {
        yenText.text = string.Format("￥{0:#,0}", yen);

        transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(10f, 10f, 10f), 0.1f);
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 180f, 0f), 0.1f);

        if (Input.GetMouseButtonDown(0))
        {
            AddYen(clickIncome);
        }
    }
}
